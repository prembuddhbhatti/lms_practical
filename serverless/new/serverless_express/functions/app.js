const express = require("express");
const serverless = require("serverless-http");

const app = express();
const router = express.Router();

router.get("/", (req, res) => {
	res.json({
		msg: "Welcome to serverless",
	});
});

router.get("/name", (req, res) => {
	res.json({
		name: "Prem",
	});
});

app.use("/", router);
module.exports.handler = serverless(app);
