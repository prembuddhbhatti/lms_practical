const express = require("express");
require("fetch");
const app = express();

const getDemo = async () => {
	await (await fetch("/.netlify/functions/demo")).json();
};
const getWelcome = async () => {
	await (await fetch("/.netlify/functions/getWelcome")).json();
};

app.get("/getDemo", (req, res) => {
	getDemo();
});
app.post("/getWelcome", (req, res) => {
	getWelcome();
});

app.listen(3000);
