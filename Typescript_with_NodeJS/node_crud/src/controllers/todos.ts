import { RequestHandler } from "express";

import { Todo } from "../models/todo";
const TODOS: Todo[] = [];

export const createTodo: RequestHandler = (req, res, next) => {
	const text = (req.body as { text: string }).text;

	const newTodo = new Todo(Math.random().toString(), text);

	TODOS.push(newTodo);

	res.status(201).json({ msg: "Created TODO", createTodo: newTodo });
};

export const getTodos: RequestHandler = (req, res, next) => {
	res.json({ todos: TODOS });
};

export const updateTodo: RequestHandler<{ id: string }> = (req, res, next) => {
	const todoid = req.params.id;

	const updateText = (req.body as { text: string }).text;

	const todoIndex = TODOS.findIndex((todo) => {
		return todo.id == todoid;
	});

	if (todoIndex < 0) {
		throw new Error("no todos");
	}

	TODOS[todoIndex] = new Todo(TODOS[todoIndex].id, updateText);

	res.json({ msg: "update", updateTodo: TODOS[todoIndex] });
};

export const deleteTodo: RequestHandler = (req, res, next) => {
	const todoid = req.params.id;

	const todoIndex = TODOS.findIndex((todo) => {
		return todo.id == todoid;
	});

	if (todoIndex < 0) {
		throw new Error("no todos");
	}
	TODOS.splice(todoIndex, 1);
	res.json({ msg: "todo deleted" });
};
