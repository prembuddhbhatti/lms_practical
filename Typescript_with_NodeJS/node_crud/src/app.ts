import { json } from "body-parser";
import express, { Request, Response, NextFunction } from "express";
import todoRoutes from "./routes/todo";
const app = express();

app.use(json());

app.use("/todos", todoRoutes);

app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
	res.status(500).json({ msg: err.message });
});

app.listen(3000);
