const path = require("path");
const cleanPlugin = require("clean-webpack-plugin");

module.exports = {
	mode: "development",
	entry: "./src/app.ts",
	devtool: "none",
	devServer: {
		static: "./",
	},
	module: {
		rules: [
			{
				test: /\.ts$/,
				use: "ts-loader",
				exclude: /node_modules/,
			},
		],
	},
	resolve: {
		extensions: [".ts", ".js"],
	},
	output: {
		filename: "bundle.js",
		path: path.resolve(__dirname, "dist"),
	},
	plugins: [new cleanPlugin.CleanWebpackPlugin()],
};
