export enum ProjectStatus {
	Active,
	Finished,
}

export class Project {
	constructor(
		public id: string,
		public title: string,
		public des: string,
		public ppl: number,
		public status: ProjectStatus
	) {}
}
