import { DragTarget } from "../models/drag_drop";
import { Project, ProjectStatus } from "../models/project";
import { Component } from "./base_components";
import { autobind } from "../decorators/autobind";
import { projectState } from "../state/project";
import { ProjectItem } from "./project_itme";

export class ProjectList extends Component<HTMLDivElement, HTMLElement> implements DragTarget {
	assignedProjects: Project[];

	constructor(private type: "active" | "finished") {
		super("project-list", "app", false, `${type}-projects`);

		this.assignedProjects = [];
		this.configure();
		this.renderContent();
	}

	@autobind
	dragOverHandler(event: DragEvent): void {
		if (event.dataTransfer && event.dataTransfer.types[0] === "text/plain") {
			event.preventDefault();
			const listEle = this.ele.querySelector("ul")!;
			listEle.classList.add("droppable");
		}
	}

	@autobind
	dropHandler(event: DragEvent): void {
		const prjId = event.dataTransfer!.getData("text/plain");
		projectState.moveProject(
			prjId,
			this.type === "active" ? ProjectStatus.Active : ProjectStatus.Finished
		);
	}

	@autobind
	dragLeaveHandler(_: DragEvent): void {
		const listEle = this.ele.querySelector("ul")!;
		listEle.classList.remove("droppable");
	}

	configure() {
		this.ele.addEventListener("dragover", this.dragOverHandler);
		this.ele.addEventListener("dragleave", this.dragLeaveHandler);
		this.ele.addEventListener("drop", this.dropHandler);
		projectState.addListner((projects: Project[]) => {
			const releventProj = projects.filter((prj) => {
				if (this.type === "active") {
					return prj.status === ProjectStatus.Active;
				}
				return prj.status === ProjectStatus.Finished;
			});
			this.assignedProjects = releventProj;
			this.renderProjects();
		});
	}
	renderContent() {
		const lsitId = `${this.type}-projects-list`;
		this.ele.querySelector("ul")!.id = lsitId;
		this.ele.querySelector("h2")!.textContent = this.type.toUpperCase() + " PROJECTS";
	}

	private renderProjects() {
		const lsitEle = document.getElementById(`${this.type}-projects-list`) as HTMLUListElement;
		lsitEle.innerHTML = "";
		for (const prjItem of this.assignedProjects) {
			new ProjectItem(this.ele.querySelector("ul")!.id, prjItem);
		}
	}
}
