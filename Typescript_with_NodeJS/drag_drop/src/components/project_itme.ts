import { Draggable } from "../models/drag_drop";
import { Project } from "../models/project";
import { Component } from "./base_components";
import { autobind } from "../decorators/autobind";

export class ProjectItem extends Component<HTMLUListElement, HTMLLIElement> implements Draggable {
	private project: Project;

	get persons() {
		if (this.project.ppl === 1) {
			return " Assigned to " + this.project.ppl.toString() + " Person";
		} else {
			return " Assigned to " + this.project.ppl.toString() + " Persons";
		}
	}
	constructor(hostId: string, project: Project) {
		super("single-project", hostId, false, project.id);
		this.project = project;

		this.configure();
		this.renderContent();
	}

	@autobind
	dragStartHanlder(event: DragEvent) {
		event.dataTransfer!.setData("text/plain", this.project.id);
		event.dataTransfer!.effectAllowed = "move";
	}

	dragEndHandler(_: DragEvent) {
		console.log("dragEnd");
	}

	configure(): void {
		this.ele.addEventListener("dragstart", this.dragStartHanlder);
		this.ele.addEventListener("dragend", this.dragEndHandler);
	}
	renderContent(): void {
		this.ele.querySelector("h2")!.textContent = this.project.title;
		this.ele.querySelector("h3")!.textContent = this.persons;
		this.ele.querySelector("p")!.textContent = this.project.des;
	}
}
