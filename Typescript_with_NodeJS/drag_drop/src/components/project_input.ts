import { Component } from "./base_components";
import { Validatable, validate } from "../util/validation";
import { autobind } from "../decorators/autobind";
import { projectState } from "../state/project";

export class ProjectInput extends Component<HTMLDivElement, HTMLFormElement> {
	titleInputEle: HTMLInputElement;
	discriptionInputEle: HTMLInputElement;
	peopleInputEle: HTMLInputElement;

	constructor() {
		super("project-input", "app", true, "user-input");
		this.titleInputEle = this.ele.querySelector("#title") as HTMLInputElement;
		this.discriptionInputEle = this.ele.querySelector("#description") as HTMLInputElement;
		this.peopleInputEle = this.ele.querySelector("#people") as HTMLInputElement;

		this.configure();
	}

	private gatherUserInput(): [string, string, number] | void {
		const title = this.titleInputEle.value;
		const des = this.discriptionInputEle.value;
		const ppl = this.peopleInputEle.value;

		const titleValidatable: Validatable = {
			value: title,
			required: true,
		};
		const desValidatable: Validatable = {
			value: des,
			required: true,
		};
		const pplValidatable: Validatable = {
			value: +ppl,
			required: true,
			min: 1,
			max: 8,
		};

		if (
			!validate(titleValidatable) ||
			!validate(desValidatable) ||
			!validate(pplValidatable)
		) {
			alert("invalid input");
			return;
		} else {
			return [title, des, +ppl];
		}
	}
	configure() {
		this.ele.addEventListener("submit", this.submitHnd);
	}
	renderContent(): void {}
	private clerInputs() {
		this.titleInputEle.value = "";
		this.discriptionInputEle.value = "";
		this.peopleInputEle.value = "";
	}

	@autobind
	private submitHnd(event: Event) {
		event.preventDefault();
		const userInput = this.gatherUserInput();
		if (Array.isArray(userInput)) {
			const [title, desc, people] = userInput;
			projectState.addProject(title, desc, people);
			console.log(title, desc, people);
		}
		this.clerInputs();
	}
}
