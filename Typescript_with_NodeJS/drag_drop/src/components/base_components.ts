export abstract class Component<T extends HTMLElement, U extends HTMLElement> {
	templateEle: HTMLTemplateElement;
	hostEle: T;
	ele: U;

	constructor(templateId: string, hostEleId: string, insertAtStart: boolean, newEleId?: string) {
		this.templateEle = <HTMLTemplateElement>document.getElementById(templateId)!; //type casting
		this.hostEle = <T>document.getElementById(hostEleId)!;

		const importedNode = document.importNode(this.templateEle.content, true);
		this.ele = importedNode.firstElementChild as U;
		if (newEleId) {
			this.ele.id = newEleId;
		}

		this.attach(insertAtStart);
	}

	private attach(insertAtStart: boolean) {
		this.hostEle.insertAdjacentElement(insertAtStart ? "afterbegin" : "beforeend", this.ele);
	}

	abstract configure(): void;
	abstract renderContent(): void;
}
