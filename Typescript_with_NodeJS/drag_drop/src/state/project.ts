import { Project, ProjectStatus } from "../models/project";

type Listner<T> = (items: T[]) => void;

class State<T> {
	protected listners: Listner<T>[] = [];
	addListner(listnersFn: Listner<T>) {
		this.listners.push(listnersFn);
	}
}

export class ProjectState extends State<Project> {
	private projects: Project[] = [];
	private static instance: ProjectState;

	private constructor() {
		super();
	}

	static getInstance() {
		if (this.instance) {
			return this.instance;
		}
		this.instance = new ProjectState();
		return this.instance;
	}

	addProject(title: string, des: string, ppl: number) {
		const newProject = new Project(
			Math.random().toString(),
			title,
			des,
			ppl,
			ProjectStatus.Active
		);
		this.projects.push(newProject);
		this.updateListners();
	}

	moveProject(projectId: string, newStatus: ProjectStatus) {
		const project = this.projects.find((prj) => prj.id === projectId);
		console.log(project);
		if (project && project.status !== newStatus) {
			project.status = newStatus;
			this.updateListners();
		}
	}

	private updateListners() {
		for (const listnersFn of this.listners) {
			listnersFn(this.projects.slice());
		}
	}
}

export const projectState = ProjectState.getInstance();
