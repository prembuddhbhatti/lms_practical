import axious from "axios";
const form = document.querySelector("form");
const addressInput = document.getElementById("address")! as HTMLInputElement;

declare var ol: any;

function searchAddress(event: Event) {
	event.preventDefault();
	document.getElementById("map")!.innerHTML = "";
	const enteredAddress = addressInput.value;
	const url =
		"https://api.mapbox.com/geocoding/v5/mapbox.places/" +
		encodeURIComponent(enteredAddress) +
		".json?limit=1&access_token=pk.eyJ1IjoicHJlbTQxMSIsImEiOiJja3l3eGZudm8wNHZqMnZtdndwa3l6Zzd2In0.1n8KG7wGVeJOApuWd5JTlA";
	axious
		.get(url)
		.then((response) => {
			const lat: number = response.data.features[0].center[1];
			const lng: number = response.data.features[0].center[0];
			console.log(lat, lng);

			new ol.Map({
				target: "map",
				layers: [
					new ol.layer.Tile({
						source: new ol.source.OSM(),
					}),
				],
				view: new ol.View({
					center: ol.proj.fromLonLat([lng, lat]),
					zoom: 16,
				}),
			});
		})
		.catch((err) => {
			console.log(err);
		});
}

form!.addEventListener("submit", searchAddress);
