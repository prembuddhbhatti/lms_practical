const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const Profile = require("../models/profile");
const Task = require("../models/task");

// localhost:3001/profile/?page=3&perPage=1&age=5
router.get("/profile", async (req, res) => {
	/*
	const query = req.query;
	 Profile.find(query)
		.then((profiles) => {
			res.json({
				confirmation: "success",
				data: profiles,
			});
		})
		.catch((err) => {
			res.json({
				confirmation: "fail",
				message: err.message,
			});
		}); */
	try {
		const query = req.query;
		const { page, perPage } = query;
		delete query["page"];
		delete query["perPage"];
		console.log(query);
		const options = {
			page: parseInt(page, 10) || 1,
			limit: parseInt(perPage, 10) || 5,
		};
		console.log(options);
		const profiles = await Profile.paginate(query, options);
		res.status(200).json(profiles);
	} catch (err) {
		res.status(500).send(err.message);
		console.log(err);
	}
});

router.get("/profile/update", (req, res) => {
	const query = req.query;
	const profileId = query.id;
	delete query["id"];
	Profile.findByIdAndUpdate(profileId, query, { new: true })
		.then((profile) => {
			res.json({
				confirmation: "success",
				data: profile,
			});
		})
		.catch((err) => {
			res.json({
				confirmation: "fail",
				message: err.message,
			});
		});
});

router.get("/profile/remove", (req, res) => {
	const query = req.query;
	Profile.findByIdAndRemove({ _id: query.id })
		.then(() => {
			res.json({
				confirmation: "success",
				data: "Profile " + query.id + " was deleted",
			});
		})
		.catch((err) => {
			res.json({
				confirmation: "fail",
				message: err.message,
			});
		});
});

router.get("/profile/:id", (req, res) => {
	const id = req.params.id;

	Profile.findById(id)
		.then((profile) => {
			res.json({
				confirmation: "success",
				data: profile,
			});
		})
		.catch((err) => {
			res.json({
				confirmation: "failed",
				message: err.message,
			});
		});
});

router.post("/profile", (req, res) => {
	Profile.create(req.body)
		.then((profile) => {
			res.json({
				confirmation: "success",
				data: profile,
			});
		})
		.catch((err) => {
			res.json({
				confirmation: "fail",
				message: err.message,
			});
		});
});

router.post("/profile/task/:id", async (req, res) => {
	const id = req.params.id;
	const task = new Task({
		...req.body,
		owner: id,
	});
	try {
		await task.save();
		res.status(201).send(task);
	} catch (e) {
		res.status(400).send(e);
	}
});

router.get("/profile/task/:id", async (req, res) => {
	const query = req.query.discription;
	const id = req.params.id;
	if (query == undefined) {
		Task.find({ owner: id })
			.sort({ createdAt: 1 })
			.sort({})
			.then((task) => {
				res.json({
					confirmation: "success",
					data: task,
				});
			})
			.catch((err) => {
				res.json({
					confirmation: "fail",
					message: "Task not found",
				});
			});
	} else {
		const rgx = new RegExp(escapeRegex(query), "gi");
		Task.find({ discription: rgx, owner: id })
			.sort({ createdAt: 1 })
			.then((task) => {
				res.json({
					confirmation: "success",
					data: task,
				});
			})
			.catch((err) => {
				res.json({
					confirmation: "fail",
					message: "Task not found",
				});
			});
	}
});

router.get("/profile/task/incomplete/:id", (req, res) => {
	const id = req.params.id;
	Task.aggregate([
		{ $match: { owner: new mongoose.Types.ObjectId(id), completed: false } },
		{ $group: { _id: "$completed", incomplete_task: { $sum: 1 } } },
		{ $project: { _id: 0, incomplete_task: 1 } },
	])
		.then((task) => {
			res.json({
				confirmation: "success",
				data: task,
			});
		})
		.catch((err) => {
			res.json({
				confirmation: "fail",
				message: "Task not found",
			});
		});
});

router.get("/profile/mytasks/:id", (req, res) => {
	const id = req.params.id;
	Profile.aggregate([
		{
			$lookup: {
				from: "tasks",
				localField: "_id",
				foreignField: "owner",
				as: "myTask",
			},
		},
	])
		.then((task) => {
			res.json({
				confirmation: "success",
				data: task,
			});
		})
		.catch((err) => {
			res.json({
				confirmation: "fail",
				message: "Task not found",
			});
		});
});

const escapeRegex = (text) => {
	return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
};

module.exports = router;
