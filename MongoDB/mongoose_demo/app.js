const express = require("express");
const path = require("path");
const mongoose = require("mongoose");
const bodyparser = require("body-parser");
const home = require("./routes/home");

//initializing express
const app = express();
mongoose
	.connect(
		"mongodb://mongooseAdmin:Admin123@localhost:27017/mongoose_demo?authSource=mongoose_demo",
		{
			// useNewUrlParser: true,
			// useCreateIndex: true,
		}
	)
	.then(() => {
		console.log("DB connected");
	})
	.catch((err) => {
		console.log(err);
	});

app.use(bodyparser.urlencoded({ extended: false }));
app.use(bodyparser.json());

app.use("/", home);
app.listen(3001, () => {
	console.log("Sever is On port: 3001!");
});
