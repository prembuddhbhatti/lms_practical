const mongoose = require("mongoose");

const Task = new mongoose.Schema({
	discription: { type: String, default: "" },
	completed: { type: Boolean, default: false },
	owner: {
		type: mongoose.Schema.Types.ObjectId,
		required: true,
		ref: "Profile",
	},
	createdAt: Date,
	updatedAt: Date,
});
Task.pre("save", function (next) {
	let now = Date.now();
	this.updatedAt = now;
	if (!this.createdAt) {
		this.createdAt = now;
	}
	next();
});
module.exports = mongoose.model("Task", Task);
