const mongoose = require("mongoose");
const Task = require("../models/task");
const paginate = require("mongoose-paginate");

const Profile = new mongoose.Schema({
	firstName: {
		type: String,
		trim: true,
		default: "",
	},
	lastName: {
		type: String,
		trim: true,
		default: "",
	},
	age: {
		type: Number,
		default: 0,
	},
	team: {
		type: String,
		trim: true,
		default: "",
	},
	position: {
		type: String,
		trim: true,
		default: "",
	},
	createdAt: Date,
	updatedAt: Date,
});

Profile.plugin(paginate);

Profile.virtual("task", {
	ref: "Task",
	localField: "_id",
	foreignField: "owner",
});

Profile.pre("save", function (next) {
	let now = Date.now();
	console.log("pre save");
	this.updatedAt = now;
	if (!this.createdAt) {
		this.createdAt = now;
	}
	next();
});

Profile.pre("remove", async function (next) {
	const profile = this;
	console.log("pre remove");
	await Task.deleteMany({ owner: profile._id });
	next();
});

module.exports = mongoose.model("Profile", Profile);
