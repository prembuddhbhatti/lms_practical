const express = require("express");
const path = require("path");

//initializing express
const app = express();

//Set view engine name and template directory path to express
app.set("view engine", "pug");

//setting path for express for views
app.set("views", path.join(__dirname, "views"));

//setting port
app.listen(3000 || process.env.PORT, () => {
	console.log("Server started listening");
});

fields = [{ name: "Technology" }, { name: "News" }, { name: "Sports" }, { name: "Travel" }];

// this will render index page
//! route /?username=prem
app.get("/", (req, res) => {
	const username = req.query.username;
	res.render("index", { user: username, fields: fields });
});

//this send simple text
//! route News/?username=prem
app.get("/News", (req, res) => {
	const username = req.query.username;
	res.send(`Welcome ${username} to docker News`);
});

//this will serve JSON
//! route Technology/?username=prem
app.get("/Technology", (req, res) => {
	const username = req.query.username;
	res.send({ name: username, technology: "NodeJs" });
});
