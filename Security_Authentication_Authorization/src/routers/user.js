const express = require("express");
const auth = require("../middleware/auth");
const User = require("../models/user");
const { sendWelcome, sendGoodBy } = require("../emails/account");
const multer = require("multer");
const sharp = require("sharp");
const router = new express.Router();

//! using async await
router.post("/users", async (req, res) => {
	const user = new User(req.body);

	try {
		await user.save();
		sendWelcome(user.email, user.name);
		const token = await user.generateAuthToken();
		res.status(201).send({ user, token });
	} catch (e) {
		res.status(400).send("Error" + e);
	}
});

router.post("/users/login", async (req, res) => {
	try {
		const user = await User.findByCred(req.body.email, req.body.password);
		const token = await user.generateAuthToken();
		res.send({ user, token });
	} catch (e) {
		res.status(404).send(e);
	}
});

router.post("/users/logout", auth, async (req, res) => {
	try {
		req.user.tokens = req.user.tokens.filter((token) => {
			return token.token !== req.token;
		});

		await req.user.save();
		res.send("logout");
	} catch (error) {
		res.status(500).send(error);
	}
});

router.post("/users/logoutall", auth, async (req, res) => {
	try {
		req.user.tokens = [];

		await req.user.save();
		res.send("loggedout of all device");
	} catch (error) {
		res.status(500).send(error);
	}
});

router.get("/users/me", auth, async (req, res) => {
	res.send(req.user);
});

/* router.get("/users/:name", async (req, res) => {
	const name = req.params.name;
	try {
		const user = await User.findOne({ name: name });
		if (!user) {
			return res.status(404).send("user not found");
		}
		res.status(200).send(user);
	} catch (e) {
		res.status(500).send("Error:", e);
	}
});
 */
router.patch("/users/me", auth, async (req, res) => {
	const update = Object.keys(req.body);
	const allowUpdate = ["name", "email", "password", "age"];
	const isValid = update.every((update) => {
		return allowUpdate.includes(update);
	});

	if (!isValid) {
		return res.status(400).send({ error: "invalid update" });
	}
	try {
		//	const user = await User.findById(req.params.id);
		update.forEach((update) => {
			req.user[update] = req.body[update];
		});

		await req.user.save();
		// const user = await User.findByIdAndUpdate(req.params.id, req.body, {
		// 	new: true,
		// 	runValidators: true,
		// });

		res.send(req.user);
	} catch (e) {
		res.status(400).send(e);
	}
});

router.delete("/users/me", auth, async (req, res) => {
	try {
		//const user = await User.findByIdAndDelete(req.user._id);
		/* if (!user) {
			return res.status(404).send("user not found");
		} */

		await req.user.remove();
		sendGoodBy(req.user.email, req.user.name);
		res.send(req.user);
	} catch (e) {
		res.status(500).send("Error:", e);
	}
});

const avatar = multer({
	limits: {
		fileSize: 1000000, //1mb
	},
	fileFilter(req, file, cb) {
		if (!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
			return cb(new Error("upload a file with extention jpg or jpeg or png"));
		}
		cb(undefined, true);
	},
});

router.post(
	"/users/me/avatar",
	auth,
	avatar.single("avatar"),
	async (req, res) => {
		const buffer = await sharp(req.file.buffer)
			.resize({ width: 200, height: 200 })
			.png()
			.toBuffer();

		req.user.avatar = buffer;
		await req.user.save();
		res.send("avatar uploaded");
	},
	(error, req, res, next) => {
		res.status(400).send({ error: error.message });
	}
);

router.delete(
	"/users/me/avatar",
	auth,
	async (req, res) => {
		req.user.avatar = undefined;
		await req.user.save();
		res.send("avatar deleted");
	},
	(error, req, res, next) => {
		res.status(400).send({ error: error.message });
	}
);

router.get("/users/:id/avatar", async (req, res) => {
	try {
		const user = await User.findById(req.params.id);
		if (!user || !user.avatar) {
			throw new Error();
		}

		res.set("Content-Type", "image/png");
		res.send(user.avatar);
	} catch (e) {
		res.status(404).send("image not found");
	}
});
module.exports = router;
