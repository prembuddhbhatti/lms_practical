const app = require("./app");
const port = process.env.PORT;

/*  //?file uploading using multer
const multer = require("multer");
const upload = multer({
	dest: "images",
	limits: {
		fileSize: 1000000, //1mb
	},
	fileFilter(req, file, cb) {
		if (!file.originalname.match(/\.(doc|docx|pdf)$/)) {
			return cb(new Error("upload pdf or word"));
		}
		cb(undefined, true);
		// cb(new Error("upload a image"));
		// cb(undefined, true);
	},
});

// app.post("/upload", upload.single("upload"), (req, res) => {
// 	res.send("uploaded");
// });


//! express error handling

// const errorMiddleware = (req, res, next) => {
// 	throw new Error("from middleware");
// }; 

app.post(
	"/upload",
	upload.single("upload"),
	(req, res) => {
		res.send();
	},
	(error, req, res, next) => {
		res.status(400).send({ error: error.message });
	}
); 
*/

/* app.use((req, res, next) => {
	if (req.method === "GET") {
		res.send("not authincated");
	} else {
		next();
	}
}); */

/*  //! creating user defined routes
const router = new express.Router();
router.get("/test", (req, res) => {
	res.send("another router");
});

app.use(router); //? regestering route
*/

app.listen(port, () => {
	console.log("server is connected:", port);
});

/* const jwt = require("jsonwebtoken");
const myFunc = async () => {
	const token = jwt.sign({ _id: "prem" }, "newcourse", { expiresIn: "1 hour" });
	console.log(token);
	const data = jwt.verify(token, "newcourse");
	console.log(data);
};
myFunc(); */
/*
const Task = require("./models/task");
const User = require("./models/user");
const main = async () => {
	const user = await User.findById("61fd2ba7a8556321c2f1d93e");
	await user.populate("tasks");
	console.log(user.tasks);
};
main(); */
