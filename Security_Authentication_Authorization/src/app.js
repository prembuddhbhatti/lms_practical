const express = require("express");
const helmet = require("helmet");
require("./db/mongoose");
const userRouter = require("./routers/user");
const taskRouter = require("./routers/task");

const app = express();
app.use(helmet());
app.use(express.json());
app.use((req, res, next) => {
	res.header("Access-Control-Allow-Origin", "*");
	res.header(
		"Access-Control-Allow-Headers",
		"Origin, X-Requested-With, Content-Type, Accept, Authorization"
	);
	if (req.method === "OPTIONS") {
		res.header("Access-Control-Allow-Methods", "GET,PUT,PATCH,POST,DELETE");
		return res.status(200).json({});
	}
	next();
});
app.use(userRouter);
app.use(taskRouter);

module.exports = app;
