const express = require("express");
const session = require("express-session");
const path = require("path");
const mongoose = require("mongoose");
const MongoDBSession = require("connect-mongodb-session")(session);
const viewPath = path.join(__dirname, "./views");

const appController = require("./controllers/appController");
const isAuth = require("./middleware/is-auth");
const app = express();
const mongoURI = "mongodb://127.0.0.1:27017/sessions_demo";
mongoose.connect(mongoURI).then((res) => {
	console.log("mongodb");
});

const store = new MongoDBSession({
	uri: mongoURI,
	collection: "mySessions",
});

app.set("view engine", "ejs");
app.set("views", viewPath);
app.use(express.urlencoded({ extended: true }));

app.use(
	session({
		secret: "Key is used to sign the cookie",
		resave: false,
		saveUninitialized: false,
		store: store,
	})
);

// Landing Page
app.get("/", appController.landing_page);

// Login Page
app.get("/login", appController.login_get);
app.post("/login", appController.login_post);

// Register Page
app.get("/register", appController.register_get);
app.post("/register", appController.register_post);

// Dashboard Page
app.get("/dashboard", isAuth, appController.dashboard_get);

app.post("/logout", appController.logout_post);

app.listen(5000, console.log("server is running on http://localhost:5000"));

// https://youtu.be/TDe7DRYK8vU
