const sgMail = require("@sendgrid/mail");
const myMail = "prem.b@simformsolutions.com";
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

const sendWelcome = (emial, name) => {
	sgMail.send({
		to: emial,
		from: myMail,
		subject: "welcome to Task app",
		text: `Welcome to the app, ${name}.\n we are Glad to have you here`,
	});
};

const sendGoodBy = (emial, name) => {
	sgMail.send({
		to: emial,
		from: myMail,
		subject: "Sad to see you going",
		text: `Goodby ${name}, \ntake a moment to let us konw why you left our app.`,
	});
};

module.exports = {
	sendWelcome,
	sendGoodBy,
};
