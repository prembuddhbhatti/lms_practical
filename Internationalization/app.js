const express = require("express");
const multer = require("multer");
const ejs = require("ejs");
const path = require("path");
const i18next = require("i18next");
const backend = require("i18next-fs-backend");
const middleware = require("i18next-http-middleware");

i18next
	.use(backend)
	.use(middleware.LanguageDetector)
	.init({
		fallbackLng: "en",
		backend: {
			loadPath: "./locals/{{lng}}/translation.json",
		},
	});
const storage = multer.diskStorage({
	destination: "./public/uploads",
	filename: function (req, file, cb) {
		cb(null, file.fieldname + "-" + Date.now() + path.extname(file.originalname));
	},
});

const upload = multer({
	storage: storage,
	limits: {
		fileSize: 100000,
	},
	fileFilter: function (req, file, cb) {
		checkFileType(file, cb);
	},
}).single("myImg");

function checkFileType(file, cb) {
	const filetypes = /jpeg|jpg|gif|png/;

	const extname = filetypes.test(path.extname(file.originalname).toLowerCase());

	const mimetype = filetypes.test(file.mimetype);

	if (mimetype && extname) {
		return cb(null, true);
	} else {
		cb("Error: Images Only");
	}
}
const app = express();
app.use(express.json());

app.use(middleware.handle(i18next));
app.set("view engine", "ejs");

app.use(express.static("./public"));

app.get("/", (req, res) => res.render("index"));

app.post("/uploads", (req, res) => {
	upload(req, res, (err) => {
		if (err) {
			res.render("index", {
				msg: err,
			});
		} else {
			if (req.file == undefined) {
				res.render("index", {
					msg: "Error: No file",
				});
			} else {
				res.render("index", {
					msg: "file uploaded",
					file: `/uploads/${req.file.filename}`,
				});
			}
			console.log(req.file);
		}
	});
});

app.get("/user", (req, res) => {
	try {
		const user = req.body.name;
		if (user == "") {
			//custom error message
			//console.log({ msg: req.t("blank").toString() });
			throw new Error(req.t("blank"));
		} else {
			res.status(200).send("Welcome:" + user);
		}
	} catch (e) {
		//console.log(e);
		res.status(400).send({ Error: e.toString() });
	}
});
const port = 3002;
app.listen(port, () => console.log("running on posrt:" + port));
