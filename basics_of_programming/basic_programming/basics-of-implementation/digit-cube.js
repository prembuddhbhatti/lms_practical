importPackage(java.io);
importPackage(java.lang);
importPackage(java.math);
importPackage(java.util);

function getSum(num) {
	let sum = 0,
		rem = 0,
		n = num;
	while (num) {
		rem = num % 10;
		sum = sum + rem;
		num = Math.floor(num / 10);
	}
	return sum;
}

function getCube(num) {
	return num * num * num;
}

var sc = new Scanner(System["in"]);

var t = sc.nextLine();
let res;
for (i = 0; i < t; i++) {
	var n = sc.next();
	var k = sc.next();
	for (j = 0; j < k; j++) {
		let sum = getSum(n);
		let res = getCube(sum);
		n = res;
	}
	System.out.println(res);
}
