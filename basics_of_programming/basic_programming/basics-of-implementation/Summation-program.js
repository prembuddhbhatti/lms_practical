//!solution in js

importPackage(java.io);
importPackage(java.lang);
importPackage(java.math);
importPackage(java.util);

function solve(n) {
	n = parseInt(n);
	let ans = 0;
	for (i = 2; i <= n / 2; i++) {
		ans += Math.floor(n / i);
	}
	ans += n + (n - Math.floor(n / 2));
	return ans;
}

var sc = new Scanner(System["in"]); // Reading input from STDIN

var t = sc.nextLine();
//System.out.println();
for (i = 1; i <= t; i++) {
	var n = parseInt(sc.nextLine());
	System.out.println(solve(n) + "\n");
}
/* process.stdin.resume();
process.stdin.setEncoding("utf-8");
var stdin_input = "";

process.stdin.on("data", function (input) {
	stdin_input += input; // Reading input from STDIN
});

process.stdin.on("end", function () {
	const input = stdin_input.split("\n");
	main(input);
});
function solve(n) {
	n = parseInt(n);
	let ans = 0;
	for (i = 2; i <= n / 2; i++) {
		ans += Math.floor(n / i);
	}
	ans += n + (n - Math.floor(n / 2));
	return ans;
}
function main(input) {
	
    // let i=2
    // while(i<input.length){
    //     const res = solve(input[i]);
    //     process.stdout.write(res+"\n");
    //     i+=2;
    // }

	for (let i = 1; i <= input[0]; i++) {
		const res = solve(input[i]);
		process.stdout.write(res + "\n");
	}
	//process.stdout.write("Hi, " + input + ".\n");       // Writing output to STDOUT
}
 */
/**
 * sample input
    1
    5
 */
/*
    sample output
    10
*/

/**
 

 */
