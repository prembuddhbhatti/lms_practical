console.log("OR:", 1 | 2);
/**
    //!bit wise OR

    here 1 = 00000001
         2 = 00000010
        
    //? it returns 1 if either of bit is 1 else 0

    res = 00000011 = 3
 */

console.log("AND:", 6 & 1);
/**
    //!bit wise AND

    here 1 = 00000001
         2 = 00000010
        
    //? it returns 1 if both bits are 1 else 0

    res = 00000000 = 3
 */
console.log("shift:", 7881 << 1);

//! real world example

// we are developing an app to give user Read,write and Execute permission
// 00000100 = 4 it means user has only Read permission
// 00000010 = 2 it means user has write permission
// 00000001 = 1 it means user has only Execute permission

const read = 4;
const write = 2;
const execute = 1;

let permission = 0;
permission = permission | read | write; //? this will add permissions

let msg = permission & read ? "yes" : "no"; //? this will check if permission exists
console.log(msg);

//!counting number of bits

let n = 4;
let ct = 0;
while (n > 0) {
	++ct;
	n >>= 1;
}
console.log("bit:", ct);

//! counting set bits
n = BigInt("494897959532893312");

console.log(n.toString(2));
rgx = /[1]/g;
console.log(n.toString(2).match(rgx).length);
let x = BigInt(0b11011011110001110110001001001110110000001100001010010000000);
console.log(x + "");

console.log(3 & 1);
