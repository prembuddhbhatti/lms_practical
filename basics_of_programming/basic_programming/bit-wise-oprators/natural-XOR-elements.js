importPackage(java.io);
importPackage(java.lang);
importPackage(java.math);
importPackage(java.util);

var sc = new Scanner(System["in"]);
var t = sc.nextInt();

while (t--) {
	let n = sc.nextInt();
	let m = n % 4;
	if (m == 3) {
		System.out.println("0");
	} else if (m == 1) {
		System.out.println("1 1");
	} else if (m == 2) {
		System.out.println("2 " + n + " 1");
	} else {
		System.out.println("1 " + n);
	}
}
/*
    sample intput

    4

    1

    2

    3

    4

    sample output

    1 1

    2 2 1

    0

    1 4
*/
