importPackage(java.io);
importPackage(java.lang);
importPackage(java.math);
importPackage(java.util);

var sc = new Scanner(System["in"]); // Reading input from STDIN

var t = sc.nextInt();

for (i = 0; i < t; i++) {
	let n = sc.nextDouble();
	//let arr = [];
	let odd = 0,
		even = 0;
	for (j = 0; j < n; j++) {
		//arr[i] = sc.nextDouble();
		a = sc.nextDouble();
		if (a & 1) {
			odd++;
		} else {
			even++;
		}
	}
	System.out.println((odd < even ? odd : even) + "");
}

/**
    sample input
    
    1
    4
    3 5 2 3

    sample output
    1
 */
