process.stdin.resume();
process.stdin.setEncoding("utf-8");
var stdin_input = "";

process.stdin.on("data", function (input) {
	stdin_input += input;
});

process.stdin.on("end", function () {
	main(stdin_input);
});

function main(input) {
	let inputArr = input.split("\n");
	for (i = 1; i < inputArr.length; i += 2) {
		let arr = inputArr[i].split(" ");
		let n = arr[0];
		let k = arr[1];
		// console.log(n);
		// console.log(k);
		//i+=1;
		let f_arr = inputArr[i + 1].split(" ");
		let d_f = 0;
		// console.log(f_arr)
		for (j = 0; j < k; j++) {
			for (x = 0; x < f_arr.length; x++) {
				if (parseInt(f_arr[x]) < parseInt(f_arr[x + 1])) {
					d_f = 1;
					f_arr.splice(x, 1);
					// console.log("f:",f_arr)
					break;
				}
			}
		}
		if (d_f == 0) {
			f_arr.pop();
		}
		//console.log(f_arr)
		process.stdout.write(f_arr.toString().replaceAll(",", " ") + "\n");
	}
}
