/* const myMap = new Map();

myMap.set("Nathan", "555-0182");
myMap.set("Jane", "315-0322");

for (let [key, value] of myMap) {
	console.log(`${key} = ${value}`);
}

console.log(myMap.get("Jane"));
 */

process.stdin.resume();
process.stdin.setEncoding("utf-8");
var stdin_input = "";

process.stdin.on("data", function (input) {
	stdin_input += input; // Reading input from STDIN
});

process.stdin.on("end", function () {
	main(stdin_input);
});

function main(input) {
	const mp = new Map();
	let inputArr = input.split("\n");
	const t = parseInt(inputArr[0]);
	//console.log(inputArr);
	//console.log(t);
	for (i = 1; i <= t; i++) {
		let dt = inputArr[i].split(" ");
		mp.set(dt[0], dt[1]);
	}
	let q = parseInt(inputArr[t + 1]);
	for (j = t + 2; j <= t + q + 1; j++) {
		process.stdout.write(mp.get(inputArr[j]) + "\n");
	}
}
