process.stdin.resume();
process.stdin.setEncoding("utf-8");
var stdin_input = "";

process.stdin.on("data", function (input) {
	stdin_input += input; // Reading input from STDIN
});

process.stdin.on("end", function () {
	main(stdin_input);
});

function getSum(arr) {
	arr = arr.sort((a, b) => {
		return b - a;
	});
	let sr = arr.slice(0, 3);
	//console.log(sr)
	let sum = 0;
	for (i = 0; i < sr.length; i++) {
		sum += sr[i];
	}
	//console.log(sum)
	return sum;
}

function main(input) {
	let inputArr = input.split("\n");
	let t = parseInt(inputArr[0]);
	for (let i = 1; i < inputArr.length; i++) {
		let n = parseInt(inputArr[i]);
		let mp = new Map();
		for (j = i + 1; j <= n + i; j++) {
			let dt = inputArr[j].split(" ");
			//console.log(dt);
			let k = dt[0];
			let v = parseInt(dt[1]);
			let c = mp.get(k);
			if (c) {
				c.push(v);
			} else {
				mp.set(k, [v]);
			}
		}
		//let sum = getSum(mp.get("A"));
		for (let [key, value] of mp) {
			let sm = getSum(value);
			mp.set(key, sm);
		}
		let arr_srt = [];
		let max = 0;
		for (let [key, value] of mp) {
			if (value >= max) {
				max = value;
			}
			//arr_srt.push([key,[value]])
		}
		for (let [key, value] of mp) {
			if (value == max) {
				arr_srt.push([key, [value]]);
			}
		}
		//console.log(max);
		//console.log(arr_srt);
		//getSum()
		let srt_arr = arr_srt.sort();
		process.stdout.write(srt_arr[0][0] + " " + srt_arr[0][1][0] + "\n");
		i += n;
		//console.log(mp);
	}
}
