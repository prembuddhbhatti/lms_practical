class HashTable {
	constructor() {
		this.table = new Array(127); //this means we can have only 127 unique IDs
		this.size = 0;
	}

	_hash(key) {
		//this will convert the key to hash
		let hash = 0;
		for (let i = 0; i < key.length; i++) {
			hash += key.charCodeAt(i);
		}
		return hash % this.table.length;
	}

	set(key, value) {
		const index = this._hash(key);
		this.table[index] = [key, value]; //storing hashed key value pair in array
		this.size++;
	}

	get(key) {
		const target = this._hash(key);
		return this.table[target];
	}

	remove(key) {
		const index = this._hash(key);

		if (this.table[index] && this.table[index].length) {
			this.table[index] = [];
			this.size--;
			return true;
		} else {
			return false;
		}
	}
}

//set
const ht = new HashTable();
ht.set("Canada", 300);
ht.set("France", 100);
ht.set("Spain", 110);

//get
console.log(ht.get("Canada")); // [ 'Canada', 300 ]
console.log(ht.get("France")); // [ 'France', 100 ]
console.log(ht.get("Spain")); // [ 'Spain', 110 ]

//remove
console.log(ht.remove("Spain")); // true
console.log(ht.get("Spain")); // undefined

//! index collision
const ht2 = new HashTable();

//this both will create same hash index
ht2.set("abc", 110);
ht2.set("cba", 192);

console.log(ht2.get("abc")); //cba, 192
console.log(ht2.get("cba")); //cba 192

//? for solution see hashing 2
