performed in Nodejs

Alice likes festivals a lot as we all do. He also likes spending money on these festivals. He spends money in buying various things on these festivals. But he has problem of forgetting. He only remembers his top three maximum spendings for any festival.
For eg, on Holi he spends 25 units on colors, 50 units on water sprays, 100 units on gifts, 150 units on sweets but he remebers only his top 3 spendings ,i.e., 50, 100 & 150.
Now as the year ends he wants to know the festival on which he spent most of his money that he can remember.

steps:
    SET stdin_input = ""

    process.stdin.on("data", function (input) {
        READ input
        SET stdin_input += input
    });

    process.stdin.on("end", function () {
        CALL main with stdin_input
    });
    
    function getSum(arr) {
        
        SET arr = arr.sort((a, b) => {
            return b - a;
        });

        SET sr = arr.slice(0, 3);
        
        SET sum = 0;
        
        FOR (i = 0; i < sr.length; i++) 
            
            sum += sr[i];

        END FOR
        
        RETURN sum;
    }
    function main(input) {
        SET inputArr = input.split("\n")
        SET t = parseInt(inputArr[0])

        FOR (let i = 1; i < inputArr.length; i++) 
            
            SET n = parseInt(inputArr[i])
            SET mp = new Map()

            FOR (j = i + 1; j <= n + i; j++) 

                SET dt = inputArr[j].split(" ")
                
                SET k = dt[0]
                SET v = parseInt(dt[1])
                SET c = mp.get(k)
                
                IF (c) THEN
                    c.push(v);
                ELSE
                    mp.set(k, [v]);
                END IF

            END FOR
            
            FOR each key,value in mp
                
                SET sm = getSum(value)
                mp.set(key, sm)

            END FOR

            SET arr_srt = []
            SET max = 0

            FOR each key,value in mp
            
                IF (value >= max) THEN
                    SET max = value
                END IF
            
            END FOR

            FOR each key,value in mp
                IF (value == max) THEN
                    arr_srt.push([key, [value]])
                END IF
            END FOR

            SET srt_arr = arr_srt.sort();
            
            PRINT srt_arr[0][0] + " " + srt_arr[0][1][0] + "\n"
            
            SET i += n;
            

        END FOR
    }