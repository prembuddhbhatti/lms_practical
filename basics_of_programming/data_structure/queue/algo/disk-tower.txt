performed in cpp

Your task is to construct a tower in N days by following these conditions:
    1) Every day you are provided with one disk of distinct size.
    2) The disk with larger sizes should be placed at the bottom of the tower.
    3) The disk with smaller sizes should be placed at the top of the tower.

The order in which tower must be constructed is as follows:
    1) You cannot put a new disk on the top of the tower until all the larger disks that are given to you get placed.

Print N lines denoting the disk sizes that can be put on the tower on the ith day.

steps:
    int main()
    {

        READ n

        SET max = n;

        SET priority_queue<int> b
        SET int a[n];

        FOR (int i = 0; i < n; i++)
        
            READ a[i]

            b.push(a[i])

            WHILE (b.top() == max)
            

                PRINT b.top() + " "
                DECREMENT max
                b.pop()

            END WHILE

            PRINT "\n"

        END FOR

        RETURN 0
    }