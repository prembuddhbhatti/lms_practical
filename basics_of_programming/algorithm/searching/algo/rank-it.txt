performed in NodeJs

steps:
    SET stdin_input = ""

    process.stdin.on("data", function (input) {
        READ input
        SET stdin_input += input
    });

    process.stdin.on("end", function () {
        CALL main with stdin_input
    });

    function main(input) {
        SET inputArr = input.split("\n")
        
        SET arr = inputArr[1].split(" ")
        SET srt_arr = arr.sort((a, b) => {
            RETURN parseInt(a) - parseInt(b);
        })
        
        SET q = parseInt(inputArr[2])
        
        FOR (i = 3; i < q + 3; i++) 
            SET ind = srt_arr.indexOf(inputArr[i]) + 1

            PRINT int + "\n"
            
        END FOR
    }