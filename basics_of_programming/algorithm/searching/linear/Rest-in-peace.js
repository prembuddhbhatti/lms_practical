process.stdin.resume();
process.stdin.setEncoding("utf-8");
var stdin_input = "";

process.stdin.on("data", function (input) {
	stdin_input += input; // Reading input from STDIN
});

process.stdin.on("end", function () {
	main(stdin_input);
});

function main(input) {
	let inputArr = input.split("\n");
	for (i = 1; i < inputArr.length; i++) {
		let n = inputArr[i];
		//console.log(n.search("21"));
		if (n.search("21") !== -1 || parseInt(n) % 21 == 0) {
			process.stdout.write("The streak is broken!\n");
		} else {
			process.stdout.write("The streak lives still in our heart!\n");
		}
	}
}
