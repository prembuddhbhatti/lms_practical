function merge(A, start, mid, end) {
	//stores the starting position of both parts in temporary variables.
	let p = start,
		q = mid + 1;

	let Arr = [],
		k = 0;

	for (i = start; i <= end; i++) {
		if (p > mid) {
			//checks if first part comes to an end or not .
			Arr[k++] = A[q++];
		} else if (q > end) {
			//checks if second part comes to an end or not
			Arr[k++] = A[p++];
		} else if (A[p] < A[q]) {
			//checks which part has smaller element.
			Arr[k++] = A[p++];
		} else {
			Arr[k++] = A[q++];
		}
	}
	for (p = 0; p < k; p++) {
		/* Now the real array has elements in sorted manner including both 
           parts.*/
		A[start++] = Arr[p];
	}
}

function merge_sort(A, start, end) {
	if (start < end) {
		mid = (start + end) / 2; // defines the current array in 2 parts .
		merge_sort(A, start, mid); // sort the 1st part of array .
		merge_sort(A, mid + 1, end); // sort the 2nd part of array.

		// merge the both parts by comparing elements of both the parts.
		merge(A, start, mid, end);
	}
}

let ar = [3, 5, 2, 6, 45, 1];
console.log(ar);
merge_sort(ar, 0, ar.length - 1);
console.log(ar);
