performed in NodeJs


steps:-
    SET stdin_input = ""

    process.stdin.on("data", function (input) {
        READ input
        SET stdin_input += input
    });

    process.stdin.on("end", function () {
        CALL main with stdin_input
    });

    SET heapsort = (arr) => {
        SET a = [...arr]
        SET l = a.length

        SET heapify = (a, i) => {
            SET left = 2 * i + 1
            SET right = 2 * i + 2
            SET max = i
            
            IF (left < l && a[left] > a[max]) THEN
                SET max = left
            END IF

            IF (right < l && a[right] > a[max]) THEN
                SET max = right
            END IF
            
            IF (max !== i) THEN
                SET [a[max], a[i]] = [a[i], a[max]]
                CALL heapify with (a, max)
            END IF
        };

        FOR (let i = Math.floor(l / 2); i >= 0; i -= 1) 
            CALL heapify with (a, i)
        END FOR

        FOR (i = a.length - 1; i > 0; i--) 
            SET [a[0], a[i]] = [a[i], a[0]]
            DECREMENT l
            CALL heapify with (a, 0)
        END FOR

        IF (a.length < 3) THEN
            PRINT "-1"
        ELSE
            SET ans = ""
            FOR (i = a.length - 1; i >= a.length - 3; i--) 
                SET ans += a[i] + " "
            END FOR
            PRINT ans
        END IF
    };
        
    function main(input) {
        SET inputArr = input.split("\n")
        SET ar = []
        FOR (let i = 1; i < inputArr.length; i++) 
            
            ar.push(parseInt(inputArr[i]))
            CALL heapsort with (ar)

        END FOR
    }