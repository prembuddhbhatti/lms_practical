function selection_sort(arr, n) {
	let min;
	let swp = 0;
	for (let i = 0; i < n - 1; i++) {
		// assuming the first element to be the minimum of the unsorted array .
		min = i;

		// gives the effective size of the unsorted  array .

		for (j = i + 1; j < n; j++) {
			if (arr[j] < arr[min]) {
				//finds the minimum element
				min = j;
			}
		}
		// putting minimum element on its proper position.
		if (min !== i) {
			temp = arr[min];
			arr[min] = arr[i];
			arr[i] = temp;
			swp++;
		}
	}
	console.log(swp);
}
let ar = [3, 5, 2, 6, 45, 1];
console.log(ar);
selection_sort(ar, ar.length);
console.log(ar);
