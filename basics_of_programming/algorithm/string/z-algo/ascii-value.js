importPackage(java.io);
importPackage(java.lang);
importPackage(java.math);
importPackage(java.util);

var sc = new Scanner(System["in"]); // Reading input from STDIN
try {
	let c = sc.next();
	System.out.println(c.charCodeAt(0) + "");
} catch (e) {
	System.out.println(e + "");
}
