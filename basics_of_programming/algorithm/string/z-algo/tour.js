process.stdin.resume();
process.stdin.setEncoding("utf-8");
var stdin_input = "";

process.stdin.on("data", function (input) {
	stdin_input += input; // Reading input from STDIN
});

process.stdin.on("end", function () {
	main(stdin_input);
});

function main(input) {
	let inputArr = input.split("\n");
	let myMap = new Map();
	let t = parseInt(inputArr[0]);
	for (i = 1; i <= t; i++) {
		let c = inputArr[i];
		myMap.set(c, i);
	}
	let arr = [];
	for (i = t + 1; i <= t * 2; i++) {
		let a = inputArr[i].split(" ");
		//console.log(a)
		arr.push(a);
	}
	//console.log(arr)
	let ans = 0;
	pos = 0;
	let q = parseInt(inputArr[t * 2 + 1]);
	for (i = t * 2 + 2; i <= t * 2 + 1 + q; i++) {
		//console.log("pos:"+pos+" mp:"+myMap.get(inputArr[i]))
		ans += parseInt(arr[pos][myMap.get(inputArr[i]) - 1]);
		pos = myMap.get(inputArr[i]) - 1;
	}

	console.log(ans);
}
