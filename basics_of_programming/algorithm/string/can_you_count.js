importPackage(java.io);
importPackage(java.lang);
importPackage(java.math);
importPackage(java.util);

var sc = new Scanner(System["in"]); // Reading input from STDIN
try {
	let t = sc.next();
	while (t--) {
		let s = sc.next();
		let a = 0,
			e = 0,
			I = 0,
			o = 0,
			u = 0;
		let res = 1,
			sum = 0;
		//System.out.println(s.charAt(0)==97);
		for (i = 0; i < s.length(); i++) {
			//System.out.println(s.charAt(i) + "");
			if (s.charAt(i) == 97) {
				a = 1;
			} else if (s.charAt(i) == 101) {
				e = 1;
			} else if (s.charAt(i) == 105) {
				I = 1;
			} else if (s.charAt(i) == 111) {
				o = 1;
			} else if (s.charAt(i) == 117) {
				u = 1;
			} else if (s.charAt(i) == 95) {
				sum = a + e + I + o + u;

				res = res * sum;
			}
			//System.out.println(sum + "");
		}
		System.out.println(res + "");
	}
} catch (e) {
	System.out.println(e + "");
}
