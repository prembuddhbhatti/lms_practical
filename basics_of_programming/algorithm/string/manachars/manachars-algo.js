let P = [];

function convertString(s) {
	let newString = "@";

	for (i = 0; i < s.length; i++) {
		newString += "#" + s.substr(i, 1);
	}

	newString += "#$";
	return newString;
}

function longestPalindrome(s) {
	let Q = convertString(s);
	P = Array(Q.length).fill(0);
	let c = 0,
		r = 0;
	f = 1;
	for (i = 1; i < Q.length - 1; i++) {
		// find the corresponding letter in the palidrome subString
		let iMirror = c - (i - c);
		if (r > i) {
			//P[i] = //min(r - i, P[iMirror]);
			if (r - 1 < P[iMirror]) {
				P[i] = r - 1;
			} else {
				P[i] = P[iMirror];
			}
		}

		// expanding around center i
		while (Q[i + 1 + P[i]] == Q[i - 1 - P[i]]) {
			P[i] += 1;
		}
		// Update c,r in case if the palindrome centered at i expands past r,
		if (i + P[i] > r) {
			c = i; // next center = i
			r = i + P[i];
		}
	}

	// Find the longest palindrome length in p.

	let maxPalindrome = 0;
	let centerIndex = 0;

	for (i = 1; i < Q.length - 1; i++) {
		if (P[i] > maxPalindrome) {
			maxPalindrome = P[i];
			centerIndex = i;
		}
	}

	console.log(maxPalindrome);
	return s.substr((centerIndex - 1 - maxPalindrome) / 2, maxPalindrome);
}

str = "abcbactr";
console.log("lp:" + longestPalindrome(str));
