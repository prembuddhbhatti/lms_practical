process.stdin.resume();
process.stdin.setEncoding("utf-8");
var stdin_input = "";

process.stdin.on("data", function (input) {
	stdin_input += input; // Reading input from STDIN
});

process.stdin.on("end", function () {
	main(stdin_input);
});

function lp(z) {
	n = z.length;
	let lps = [];
	lps.push(0);
	for (i = 1; i < n; i++) {
		j = lps[i - 1];
		while (j > 0 && z[i] != z[j]) {
			j = lps[j - 1];
		}
		if (z[i] == z[j]) {
			j++;
		}
		lps[i] = j;
	}
	return lps;
}

function kmp(ptr, txt) {
	let n = txt.length;
	let m = ptr.length;
	let lps = lp(ptr);
	let i = 0,
		j = 0,
		ctr = 0;
	//console.log(n);
	while (i < n) {
		if (ptr[j] == txt[i]) {
			i++;
			j++;
		}
		if (j == m) {
			ctr++;
			j = lps[j - 1];
		} else if (i < n && ptr[j] != txt[i]) {
			if (j != 0) {
				j = lps[j - 1];
			} else {
				i++;
			}
		}
	}
	//console.log(ctr);
	return ctr;
}

function main(input) {
	let inputArr = input.split("\n");
	let ptr = inputArr[0];
	let str = inputArr[1];
	console.log(kmp(ptr, str));
}
