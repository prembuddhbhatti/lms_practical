process.stdin.resume();
process.stdin.setEncoding("utf-8");
var stdin_input = "";

process.stdin.on("data", function (input) {
	stdin_input += input; // Reading input from STDIN
});

process.stdin.on("end", function () {
	main(stdin_input);
});

function main(input) {
	let inputArr = input.split("\n");
	let str = inputArr[1];
	const myMap = new Map();
	for (i = 0; i < inputArr[0]; i++) {
		let e = inputArr[1][i];
		let m = myMap.get(e);
		if (m) {
			myMap.set(e, m + 1);
		} else {
			myMap.set(e, 1);
		}
	}
	//console.log(myMap)
	const srt_mp = new Map([...myMap.entries()].sort((a, b) => b[1] - a[1]));
	for (let [key, value] of srt_mp) {
		console.log(`${value}`);
		break;
	}
}
