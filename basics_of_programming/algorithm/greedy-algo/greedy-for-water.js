/*
// Below is a sample code to process input from STDIN.
// Equivalent in effect to the Java declaration import java.io.*;
importPackage(java.io);
importPackage(java.lang);
importPackage(java.math);
importPackage(java.util);

var sc = new Scanner(System['in']);					// Reading input from STDIN

var my_name = sc.nextLine();
System.out.println("Hi, " + my_name + ".");			// Writing output to STDOUT
// End of input processing code.

// Warning: Printing unwanted or ill-formatted data to output will cause the test cases to fail
*/

// Write your code here
importPackage(java.io);
importPackage(java.lang);
importPackage(java.math);
importPackage(java.util);

var sc = new Scanner(System["in"]); // Reading input from STDIN

function bubble_sort(arr, n) {
	let temp;
	let swp = 0;
	for (let k = 0; k < n - 1; k++) {
		// (n-k-1) is for ignoring comparisons of elements which have already been compared in earlier iterations

		for (let i = 0; i < n - k - 1; i++) {
			if (arr[i] > arr[i + 1]) {
				// here swapping of positions is being done.
				swp++;
				temp = arr[i];
				arr[i] = arr[i + 1];
				arr[i + 1] = temp;
			}
		}
	}
	//console.log(swp);
	//return arr;
}

try {
	let t = sc.next();
	while (t--) {
		let n = sc.nextInt();
		let x = sc.nextInt();
		let btl_arr = [];
		for (i = 0; i < n; i++) {
			btl_arr.push(sc.nextInt());
		}
		//System.out.println(btl_arr.toString())

		bubble_sort(btl_arr, btl_arr.length);
		let ttl = 0;
		let ct = 0;
		for (i = 0; i < n; i++) {
			ttl += btl_arr[i];
			if (ttl >= x) {
				ttl -= btl_arr[i];
				break;
			} else {
				ct++;
			}
		}
		System.out.println(ct + "");
	}
} catch (e) {
	System.out.println(e + "");
}
