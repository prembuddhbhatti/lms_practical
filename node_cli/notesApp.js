//const fs = require("fs"); //?This will include FileSystem module in this file
const yargs = require("yargs");
const { readNote } = require("./modules/notes.js");
const notes = require("./modules/notes.js");
//const validator = require("validator");
//const chalk = require("chalk");
//const fn = "mynotes.txt";
//const cmd = process.argv[0];
/* //this creates a new file or overright the file if it exists
fs.writeFileSync(fn, "i'll keep yor notes!");

//?this will appen file
fs.appendFileSync(fn, "\ni'm appended");

console.log(app());

console.log(validator.isEmail("prem@abc.com"));
console.log(chalk.red.underline.bgGreen("chalk"));
console.log(chalk.red.underline.bgGreen.inverse("chalk"));
 */
/* //!yargs is used for managing command line arguments
console.log(process.argv);
console.log(yargs.argv);
if (cmd === "add") {
	console.log("adding");
} else if (cmd === "remove") {
	console.log("removing");
} */

//setting yargs version
yargs.version("1.1.0");

//creating add command
yargs.command({
	command: "add",
	describe: "Add a new note",
	builder: {
		title: {
			describe: "Note title",
			demandOption: true, //this makes this as required
			type: "string", //this specifies the input type
		},
		body: {
			describe: "Note body",
			demandOption: true,
			type: "string",
		},
	},
	handler(argv) {
		//? es6 method defination
		notes.addNotes(argv.title, argv.body);
	},
});

//creating remove command
yargs.command({
	command: "remove",
	discribe: "remove notes",
	builder: {
		title: {
			describe: "Note title",
			demandOption: true, //this makes this as required
			type: "string", //this specifies the input type
		},
	},
	handler(argv) {
		notes.removeNote(argv.title);
	},
});

//creating list command
yargs.command({
	command: "list",
	discribe: "list notes",
	handler() {
		notes.listNotes();
	},
});

//creating read command
yargs.command({
	command: "read",
	discribe: "read a note",
	builder: {
		title: {
			describe: "Note title",
			demandOption: true, //this makes this as required
			type: "string", //this specifies the input type
		},
	},
	handler(argv) {
		readNote(argv.title);
	},
});
yargs.parse();
