console.log("module/app");
const fs = require("fs");
const chalk = require("chalk");
const file = "myNotes.json";
const getNotes = () => {
	return "notes...";
};

const addNotes = (title, body) => {
	const notes = loadNotes();
	const duplicate = notes.find((note) => note.title === title);
	debugger;
	if (!duplicate) {
		notes.push({
			title: title,
			body: body,
		});
		saveNotes(notes);
		console.log(chalk.bgGreen.black("new note added"));
	} else {
		console.log(chalk.bgRed.black("duplicate note"));
	}
};

const removeNote = (title) => {
	const notes = loadNotes();
	const notesKeep = notes.filter((note) => note.title !== title);

	if (notes.length === notesKeep.length) {
		console.log(chalk.bgRed.black("no note with title: '" + title + "' found!"));
	} else {
		saveNotes(notesKeep);
		console.log(chalk.bgGreen.black("removed note with title:" + title));
	}
};

const listNotes = () => {
	const notes = loadNotes();

	if (notes.length > 0) {
		console.log(chalk.bgGreen.black("Your notes:"));
		notes.forEach((note) => {
			console.log(chalk.blue(note.title));
		});
	} else {
		console.log(chalk.bgRed.black("No notes found"));
	}
};

const readNote = (title) => {
	const notes = loadNotes();
	const note = notes.find((note) => note.title === title);
	if (note) {
		console.log(chalk.bgGreen.black(note.title));
		console.log(chalk.blue(note.body));
	} else {
		console.log(chalk.bgRed.black("No note found with title: " + title));
	}
};
const saveNotes = (notes) => {
	const dataJSON = JSON.stringify(notes);
	fs.writeFileSync(file, dataJSON);
};

const loadNotes = () => {
	try {
		const dataBuffer = fs.readFileSync(file);
		const dataJSON = dataBuffer.toString();
		return JSON.parse(dataJSON);
	} catch (e) {
		return [];
	}
};
module.exports = {
	getNotes: getNotes,
	addNotes: addNotes,
	removeNote: removeNote,
	listNotes: listNotes,
	readNote: readNote,
};
