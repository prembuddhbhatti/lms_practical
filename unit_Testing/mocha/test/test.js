// Requiring module
const assert = require("assert");
const app = require("../app");
const request = require("supertest");

describe("routes test", () => {
	describe("news route", () => {
		beforeEach(() => {
			console.log("checking News route");
		});

		it(`Is returning Welcome prem to Express News`, function (done) {
			request(app)
				.get("/News/?username=prem")
				.expect(200)
				.then((res) => {
					assert(res.text, "Welcome prem to Express News");
					done();
				})
				.catch((err) => done(err));
		});
	});

	describe("Technology route", () => {
		beforeEach(() => {
			console.log("checking News route");
		});

		it(`Is returning json object`, function (done) {
			request(app)
				.get("/News/?username=prem")
				.expect(200)
				.then((res) => {
					console.log(res.text);
					assert.ok("Welcome prem to Express News" == res.text);
					done();
				})
				.catch((err) => done(err));
		});
	});
});

// We can group similar tests inside a describe block
describe("Simple Calculations", () => {
	before(() => {
		console.log("This part executes once before all tests");
	});

	after(() => {
		console.log("This part executes once after all tests");
	});

	// We can add nested blocks for different tests
	describe("Test1", () => {
		beforeEach(() => {
			console.log("executes before every test");
		});

		it("Is returning 5 when adding 2 + 3", () => {
			assert.equal(2 + 3, 5);
		});

		it("Is returning 6 when multiplying 2 * 3", () => {
			assert.equal(2 * 3, 6);
		});
	});

	describe("Test2", () => {
		beforeEach(() => {
			console.log("executes before every test");
		});

		it("Is returning 4 when adding 2 + 3", () => {
			assert.equal(2 + 3, 4);
		});

		it("Is returning 8 when multiplying 2 * 4", () => {
			assert.equal(2 * 4, 8);
		});
	});
});
