const request = require("supertest");
const app = require("../src/app");
const User = require("../src/models/user");
const { userOne, userOneId, setUpDB } = require("./fixtures/db");

beforeEach(setUpDB);

test("New user signup", async () => {
	const response = await request(app)
		.post("/users")
		.send({
			name: "test",
			email: "test@gmail.com",
			password: "test@1234",
		})
		.expect(201);

	//checking if database was changed correctly
	const user = await User.findById(response.body.user._id);
	expect(user).not.toBeNull();

	//checking response
	expect(response.body).toMatchObject({
		user: {
			name: "test",
			email: "test@gmail.com",
		},
		token: user.tokens[0].token,
	});

	//checking if password was encrypted

	expect(user.password).not.toBe("test@1234");
});

test("Login existing user", async () => {
	const response = await request(app)
		.post("/users/login")
		.send({
			email: userOne.email,
			password: userOne.password,
		})
		.expect(200);

	const user = await User.findById(userOneId);
	expect(response.body.token).toBe(user.tokens[1].token);
});

test("should not login with wrong credentials", async () => {
	await request(app)
		.post("/users/login")
		.send({
			email: userOne.email,
			password: "wrong@123",
		})
		.expect(404);
});

test("Get User Profile", async () => {
	await request(app)
		.get("/users/me")
		.set("Authorization", `Bearer ${userOne.tokens[0].token}`)
		.send()
		.expect(200);
});

test("User Profile when unauthorized", async () => {
	await request(app).get("/users/me").send().expect(401);
});

test("delete User Profile", async () => {
	await request(app)
		.delete("/users/me")
		.set("Authorization", `Bearer ${userOne.tokens[0].token}`)
		.send()
		.expect(200);

	//cheking if user is actually deleted
	const user = await User.findById(userOneId);
	expect(user).toBeNull();
});

test("delete User Profile when unauthorized", async () => {
	await request(app).delete("/users/me").send().expect(401);
});

test("upload avatar", async () => {
	await request(app)
		.post("/users/me/avatar")
		.set("Authorization", `Bearer ${userOne.tokens[0].token}`)
		.attach("avatar", "tests/fixtures/avatar.jpeg")
		.expect(200);

	const user = await User.findById(userOneId);
	expect(user.avatar).toEqual(expect.any(Buffer));
});

test("update user profile", async () => {
	await request(app)
		.patch("/users/me")
		.set("Authorization", `Bearer ${userOne.tokens[0].token}`)
		.send({
			name: "test Update",
		})
		.expect(200);

	const user = await User.findById(userOneId);
	expect(user.name).toBe("test Update");
});

test("should not update invalid field in user profile", async () => {
	await request(app)
		.patch("/users/me")
		.set("Authorization", `Bearer ${userOne.tokens[0].token}`)
		.send({
			update: "test Update",
		})
		.expect(400);
});
