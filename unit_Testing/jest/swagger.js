const swaggerAutogen = require("swagger-autogen")();
//const u = require("./routers/user")
const outputFile = "./swagger_output.json";
const endpointsFiles = ["./src/app.js"];

swaggerAutogen(outputFile, endpointsFiles).then(() => {
	require("./index.js");
});
