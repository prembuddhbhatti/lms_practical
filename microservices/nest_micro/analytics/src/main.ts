import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.connectMicroservice({
    transport: Transport.TCP,
    Option: {
      port: 5000 | 5001,
    },
  });
  await app.startAllMicroservices();
  app.listen(5000);
}
bootstrap();
