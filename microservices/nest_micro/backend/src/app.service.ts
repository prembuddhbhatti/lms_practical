import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { CreateUser } from './create-user.dto';
import { createUserEvent } from './create-user.event';

@Injectable()
export class AppService {
  private readonly users: any[] = [];

  constructor(
    @Inject('COMMUNICATION') private readonly communicationClient: ClientProxy,
    @Inject('ANALYTICS') private readonly analyticsClient: ClientProxy,
  ) {}

  getHello(): string {
    return 'Hello World!';
  }

  createUser(createUser: CreateUser) {
    this.users.push(createUser);
    this.communicationClient.emit(
      'user_created',
      new createUserEvent(createUser.email),
    );
    this.analyticsClient.emit(
      'user_created',
      new createUserEvent(createUser.email),
    );
  }
}
