import { Module } from '@nestjs/common';

import { CustomerModule } from './customer/customer.module';
import { CustomersController } from './customer/controllers/customers/customers.controller';
import { CustomersService } from './customer/services/customers/customers.service';
import { UsersModule } from './users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './auth/auth.module';
import entities from './typeorm';
import { PassportModule } from '@nestjs/passport';

@Module({
  imports: [
    CustomerModule,
    UsersModule,
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'prem',
      password: 'Prem@12345',
      database: 'nest_demo',
      entities: entities,
      synchronize: true,
    }),
    AuthModule,
    PassportModule.register({
      session: true,
    }),
  ],
  controllers: [CustomersController],
  providers: [CustomersService],
})
export class AppModule {}
