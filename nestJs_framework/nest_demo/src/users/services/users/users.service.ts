import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User as UserEntity } from 'src/typeorm';
import { CreateUserDto } from 'src/users/dto/CreateUser.dto';

import { User, SerilizedUser } from 'src/users/types/index';
import { encodePassword } from 'src/utils/bcrypt';
import { Repository } from 'typeorm';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
  ) {}
  private users: User[] = [
    { id: 1, username: 'prem', password: 'prem' },
    { id: 2, username: 'raj', password: 'raj' },
    { id: 3, username: 'het', password: 'het' },
    { id: 4, username: 'arkit', password: 'arkit' },
  ];

  getUsers() {
    return this.users.map((user) => new SerilizedUser(user));
  }

  getUserByUsername(username: string) {
    return this.users.find((user) => user.username === username);
  }

  getUserById(id: number) {
    return this.users.find((user) => user.id === id);
  }

  createUser(createUserDto: CreateUserDto) {
    const password = encodePassword(createUserDto.password);
    const newUser = this.userRepository.create({ ...createUserDto, password });
    return this.userRepository.save(newUser);
  }
  findUserByUsername(username: string) {
    return this.userRepository.findOne({ where: { username: username } });
  }

  findUserById(id: number) {
    return this.userRepository.findOne({ where: { id: id } });
  }
}
