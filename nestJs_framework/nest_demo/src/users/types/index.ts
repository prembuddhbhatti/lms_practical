import { Exclude } from 'class-transformer';

export interface User {
  id: number;
  username: string;
  password: string;
}

export class SerilizedUser {
  id: number;
  username: string;

  @Exclude()
  password: string;

  constructor(partial: Partial<SerilizedUser>) {
    Object.assign(this, partial);
  }
}
