import { Inject } from '@nestjs/common';
import { PassportSerializer } from '@nestjs/passport';
import { User } from 'src/typeorm';
import { UsersService } from 'src/users/services/users/users.service';

export class SessionSerilizer extends PassportSerializer {
  constructor(
    @Inject('USER_SREVICE') private readonly userService: UsersService,
  ) {
    super();
  }
  serializeUser(user: User, done: (error, user: User) => void) {
    done(null, user);
  }
  async deserializeUser(user: User, done: (error, user: User) => void) {
    const userDb = await this.userService.findUserById(user.id);
    return userDb ? done(null, userDb) : done(null, null);
  }
}
