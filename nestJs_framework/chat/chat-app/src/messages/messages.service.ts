import { Injectable } from '@nestjs/common';
import { CreateMessageDto } from './dto/create-message.dto';
import { Message } from './entities/message.entity';

@Injectable()
export class MessagesService {
  messages: Message[] = [{ name: 'admin', text: 'hii' }];
  clientToUser = {};

  create(createMessageDto: CreateMessageDto, clientId: string) {
    console.log(
      '🚀 ==> file: messages.service.ts ==> line 11 ==> MessagesService ==> create ==> clientId',
      clientId,
    );
    const message = {
      name: this.clientToUser[clientId],
      text: createMessageDto.text,
    };
    this.messages.push(message);
    console.log(
      '🚀 ==> file: messages.service.ts ==> line 18 ==> MessagesService ==> create ==> message',
      message,
    );
    return message;
  }

  findAll() {
    return this.messages;
  }

  identify(name: string, clientId: string) {
    this.clientToUser[clientId] = name;
    return Object.values(this.clientToUser);
  }

  getClientName(clientId: string) {
    return this.clientToUser[clientId];
  }
}
