import { Injectable } from '@nestjs/common';

export type User = {
  id: number;
  name: string;
  username: string;
  password: string;
};

@Injectable()
export class UsersService {
  private readonly users: User[] = [
    {
      id: 1,
      name: 'prem',
      username: 'prem123',
      password: 'prem123',
    },
    {
      id: 2,
      name: 'nishant',
      username: 'nishant123',
      password: 'nishant123',
    },
  ];

  async findOne(username: string): Promise<User | undefined> {
    return this.users.find((user) => user.username === username);
  }
}
