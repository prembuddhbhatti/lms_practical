import { Inject, Injectable } from '@nestjs/common';
import { UsersService } from '../../../users/services/users/users.service';

@Injectable()
export class AuthService {
  constructor(
    @Inject('USER_SERVICE') private readonly userService: UsersService,
  ) {}
  async validateUser(name: string, password: string) {
    console.log('Validate user');
    const userDB = await this.userService.findUser(name);
    if (userDB && userDB.password === password) {
      return userDB;
    }
    return null;
  }
}
