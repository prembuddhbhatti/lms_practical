import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersEntity } from 'src/typeorm';
import { UsersService } from '../users/services/users/users.service';

import { AuthController } from './controllers/auth/auth.controller';
import { AuthService } from './services/auth/auth.service';
import { LoaclStrategy } from './utils/LocalStrategy';

@Module({
  imports: [TypeOrmModule.forFeature([UsersEntity]), PassportModule],
  providers: [
    {
      provide: 'AUTH_SERVICE',
      useClass: AuthService,
    },
    {
      provide: 'USER_SERVICE',
      useClass: UsersService,
    },
    LoaclStrategy,
  ],
  controllers: [AuthController],
})
export class AuthModule {}
