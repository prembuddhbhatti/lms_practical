import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { TasksEntity } from './Tasks';

@Entity()
export class UsersEntity {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'user_id',
  })
  id: number;

  @Column({
    nullable: false,
  })
  name: string;

  @Column({
    nullable: false,
  })
  password: string;

  @Column()
  createdAt: Date;

  @OneToMany(() => TasksEntity, (task) => task.user)
  tasksEntity: TasksEntity[];
}
