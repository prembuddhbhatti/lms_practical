import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { UsersEntity } from './Users';

@Entity()
export class TasksEntity {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'task_id',
  })
  id: number;

  @Column({
    nullable: false,
  })
  title: string;

  @Column({
    nullable: false,
  })
  status: string;

  @Column()
  createdAt: Date;

  @ManyToOne(() => UsersEntity, (user) => user.tasksEntity, {
    onDelete: 'CASCADE',
  })
  @JoinColumn({
    name: 'user_id',
  })
  user: UsersEntity;
}
