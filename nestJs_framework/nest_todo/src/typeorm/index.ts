import { TasksEntity } from './Tasks';
import { UsersEntity } from './Users';
export { TasksEntity };
export { UsersEntity };
const entities = { TasksEntity, UsersEntity };
export default entities;
