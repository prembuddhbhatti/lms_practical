export interface Task {
  title: string;
  status: string;
  createdAt: Date;
}
