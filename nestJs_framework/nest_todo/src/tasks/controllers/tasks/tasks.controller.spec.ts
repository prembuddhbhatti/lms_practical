import { Test, TestingModule } from '@nestjs/testing';

import { TasksService } from '../../services/tasks/tasks.service';

import { TasksController } from './tasks.controller';

describe('TasksController', () => {
  let controller: TasksController;
  const mockTasksService = {
    createTask: jest.fn((id, dto) => {
      return {
        id: 7,
        createdAt: Date(),
        ...dto,
      };
    }),

    updateTaskByTitle: jest.fn().mockImplementation((title, status) => ({
      title,
      status,
    })),

    findTaskByTitle: jest.fn((title) => {
      return {
        title: title,
        status: String,
        createdAt: expect.anything(),
        id: expect.any(Number),
      };
    }),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TasksController],
      providers: [TasksService],
    })
      .overrideProvider(TasksService)
      .useValue(mockTasksService)
      .compile();

    controller = module.get<TasksController>(TasksController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create a task', () => {
    const dto = {
      title: 'test',
      status: 'InProgress',
    };

    expect(controller.createTask('1', dto)).toEqual({
      title: 'test',
      status: 'InProgress',
      createdAt: expect.anything(),
      id: expect.any(Number),
    });
    expect(mockTasksService.createTask).toHaveBeenCalledWith(dto);
  });

  it('should update a task', () => {
    const dto = {
      status: 'Completed',
    };
    expect(controller.updateTask('test', dto)).toEqual({
      title: 'test',
      ...dto,
    });
  });

  it('should return a task', () => {
    expect(controller.getTasks('1', 'test')).toEqual({
      title: 'test',
      status: String,
      createdAt: expect.anything(),
      id: expect.any(Number),
    });
  });
});
