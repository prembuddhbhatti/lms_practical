import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';

import { CreateTaskDto } from '../../dtos/CreateTask.dto';
import { UpdateTaskDto } from '../../dtos/UpdateTask.dto';

import { TasksService } from '../../services/tasks/tasks.service';

@Controller('tasks')
export class TasksController {
  constructor(private tasksServices: TasksService) {}
  @Get(':id/:title')
  getTasks(
    @Param('id', ParseIntPipe) userid: string,
    @Param('title') title: string,
  ) {
    const task = this.tasksServices.findTaskByTitle(+userid, title);
    if (task) {
      return task;
    } else {
      throw new HttpException('Task not found', HttpStatus.NOT_FOUND);
    }
  }

  @Get('getall/:id')
  getAllTask(@Param('id', ParseIntPipe) userid: string) {
    console.log('get all');

    return this.tasksServices.getAllTask(+userid);
  }

  @Post(':id')
  @UsePipes(ValidationPipe)
  createTask(
    @Param('id', ParseIntPipe) userid: string,
    @Body() createTaskDto: CreateTaskDto,
  ) {
    const task = this.tasksServices.createTask(+userid, createTaskDto);
    if (task === null) {
      return 'user not found';
    }
    return task;
  }

  @Patch(':title')
  @UsePipes(ValidationPipe)
  updateTask(@Param('title') title: string, @Body() updateTask: UpdateTaskDto) {
    const task = this.tasksServices.updateTaskByTitle(title, updateTask.status);
    if (task) {
      return task;
    } else {
      throw new HttpException('Task not found', HttpStatus.NOT_FOUND);
    }
  }
}
