import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersService } from 'src/users/services/users/users.service';
import { ValidateUsers } from '../middlewares/validate-user.middleware';
import { TasksEntity, UsersEntity } from '../typeorm/index';
import { TasksController } from './controllers/tasks/tasks.controller';
import { TasksService } from './services/tasks/tasks.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([TasksEntity]),
    TypeOrmModule.forFeature([UsersEntity]),
  ],
  controllers: [TasksController],
  providers: [
    TasksService,
    {
      provide: 'USER_SERVICE',
      useClass: UsersService,
    },
  ],
})
export class TasksModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(ValidateUsers).forRoutes(TasksController);
  }
}
