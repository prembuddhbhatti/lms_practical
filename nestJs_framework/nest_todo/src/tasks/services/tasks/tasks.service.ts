import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UsersService } from 'src/users/services/users/users.service';
import { Repository } from 'typeorm';
import { TasksEntity, UsersEntity } from '../../../typeorm/index';
import { CreateTaskDto } from '../../dtos/CreateTask.dto';

@Injectable()
export class TasksService {
  constructor(
    @InjectRepository(TasksEntity)
    private readonly tasksRepository: Repository<TasksEntity>,
    @Inject('USER_SERVICE') private readonly usersService: UsersService,
    @InjectRepository(UsersEntity)
    private readonly usersRepository: Repository<UsersEntity>,
  ) {}

  async findTaskByTitle(userId: number, title: string) {
    const task = await this.tasksRepository.findOne({
      where: { title: title, user: { id: userId } },
    });
    return task;
  }

  async createTask(userId: number, taskDto: CreateTaskDto) {
    const user = await this.usersService.findUserById(+userId);
    if (!user) {
      return null;
    }
    console.log(user);
    const createdAt = new Date();

    const newTask = this.tasksRepository.create({
      ...taskDto,
      createdAt,
      user,
    });
    const task = this.tasksRepository.save(newTask);

    return task;
  }

  getAllTask(id: number) {
    return this.tasksRepository.find({ where: { user: { id: id } } });
  }

  async updateTaskByTitle(title: string, status: string) {
    const task = await this.tasksRepository.findOne({
      where: { title: title },
    });
    if (task) {
      const res = this.tasksRepository.save({ ...task, status: status });
      return res;
    }
  }
}
