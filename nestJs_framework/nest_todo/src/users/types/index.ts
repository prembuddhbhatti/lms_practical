import { Exclude } from 'class-transformer';

export interface User {
  name: string;
  password: string;
}

export class SerilizedUser {
  name: string;
  createAt: string;

  @Exclude()
  password: string;
}
