import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UsersEntity } from 'src/typeorm';
import { CreateUserDto } from 'src/users/dtos/CreateUser.dto';

import { Repository } from 'typeorm';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UsersEntity)
    private readonly usersRepository: Repository<UsersEntity>,
  ) {}
  getAll() {
    return this.usersRepository.find();
  }

  createNewUser(userDto: CreateUserDto) {
    const createdAt = new Date();

    const newUser = this.usersRepository.create({ ...userDto, createdAt });
    return this.usersRepository.save(newUser);
  }

  findUser(name: string) {
    return this.usersRepository.findOne({ where: { name: name } });
  }

  findUserById(id: number) {
    return this.usersRepository.findOne({ where: { id: id } });
  }
}
